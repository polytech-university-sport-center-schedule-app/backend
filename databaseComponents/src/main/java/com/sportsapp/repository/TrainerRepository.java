package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Trainer;

@Repository
public interface TrainerRepository extends CrudRepository<Trainer, Long> {
}
