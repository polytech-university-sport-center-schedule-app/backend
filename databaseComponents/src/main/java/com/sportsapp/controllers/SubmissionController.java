package com.sportsapp.controllers;

import com.sportsapp.entity.Submission;
import com.sportsapp.repository.SubmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController("submission")
public class SubmissionController {

    @Autowired
    SubmissionRepository submissionRepository;

    @GetMapping()
    public List<Submission> getAllUncheckedSubmissions() {
        return ((List<Submission>)submissionRepository.findAll()).stream()
                                                                 .filter(submission -> submission.getStatus()==null)
                                                                 .collect(Collectors.toList()
                );
    }

    @PostMapping
    public Submission changeSubmissionStatus(@RequestBody Submission submission) {
        return submissionRepository.save(submission);
    }
}
