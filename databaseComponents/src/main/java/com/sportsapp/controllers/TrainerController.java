package com.sportsapp.controllers;

import com.sportsapp.entity.Trainer;
import com.sportsapp.repository.SubmissionRepository;
import com.sportsapp.repository.TrainerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/trainer")
public class TrainerController {

    @Autowired
    TrainerRepository trainerRepository;

    @Autowired
    SubmissionRepository submissionRepository;

    @PostMapping()
    public Trainer addTrainer(@RequestBody Trainer trainer, HttpServletResponse httpServletResponse) {
        log.info(trainer.getFirstName() + trainer.getLastName());
        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        return trainerRepository.save(trainer);
    }

    @GetMapping()
    public List<Trainer> getAllTrainers(HttpServletResponse httpServletResponse) {
        log.info("CORS!!!!!!!!!!!!!!!!!!!!!!!!");
        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        return (List<Trainer>) trainerRepository.findAll();
    }

    @GetMapping("/{id}")
    public Trainer getTrainer(@PathVariable Long id) {
        return trainerRepository.findById(id).orElseThrow(() -> new NullPointerException());
    }

    @DeleteMapping("/{id}")
    public void deleteTrainer(@PathVariable Long id) {
        trainerRepository.deleteById(id);
    }
}
