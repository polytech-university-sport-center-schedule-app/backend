package com.sportsapp.entity;

import java.util.Arrays;
import java.util.List;

import javax.persistence.*;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Trainers")
@Getter @Setter @NoArgsConstructor 
public class Trainer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String lastName;
	private String firstName;
	private String secondName;

	

	
//	@Column(name="photo_file_path")
//	private String photoFilePath;
//
//	@ManyToOne
//	@JoinColumn(name="specializations_id")
//	private Specialization specialization;
//
//	@ManyToMany(cascade = CascadeType.ALL)
//	@JoinTable(
//			name="groups_trainers",
//			joinColumns = @JoinColumn(name = "trainers_id"),
//			inverseJoinColumns = @JoinColumn(name="groups_id"))
//	private List<Group> groupList;
//
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
	 	result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((secondName == null) ? 0 : secondName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		//result = prime * result + ((photoFilePath == null) ? 0 : photoFilePath.hashCode());
		//result = prime * result + ((specialization == null) ? 0 : specialization.hashCode());
		//result = prime * result + ((groupList == null) ? 0 : groupList.hashCode());

		return result;
	}
//
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Trainer other = (Trainer) obj;
		if(id == null) {
			if(other.id != null)
				return false;
		} else if(!id.equals(other.id))
			return false;
		if(firstName == null) {
			if(other.firstName != null)
				return false;
		} else if(!firstName.equals(other.firstName))
			return false;
		if(secondName == null) {
			if(other.secondName != null)
				return false;
		} else if(!secondName.equals(other.secondName))
			return false;
		if(lastName == null) {
			if(other.lastName != null)
				return false;
		} else if(!lastName.equals(other.lastName))
			return false;
//		if(specialization == null) {
//			if(other.specialization != null)
//				return false;
//		} else if(!specialization.equals(other.specialization))
//			return false;
//		if(photoFilePath == null) {
//			if(other.photoFilePath != null)
//				return false;
//		} else if(!photoFilePath.equals(other.photoFilePath))
//			return false;
//		if(groupList == null) {
//			if(other.groupList != null)
//				return false;
//		} else if(!groupList.equals(other.groupList))
//			return false;
//
		return true;
	}
//
	@Override
	public String toString() {
		return "Trainer [id=" + id + ", first name=" + firstName  +
				", last name =" + lastName  + "]";
	}
	
	
	
}
