///** NOTE:
// * means the same as "sport" at https://app.dbdesigner.net/designer/schema/0-trpo-016e7e20-38f8-47bd-8958-0171119c7a7b
// * renamed just for matching a front-end group filter criteria
// * (field "Specialization" inside selection form in submission.html)
// */
//
//package com.sportsapp.entity;
//
//import java.util.Arrays;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import lombok.Data;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//
//@Entity
//@Data
//@Table(name = "Specializations")
//@Getter @Setter @NoArgsConstructor
//public class Specialization {
//
//	@Id
//	@SequenceGenerator(name = "specializationIdSequence", sequenceName = "specializationIdSequence", allocationSize = 1, initialValue = 1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "specializationIdSequence")
//	@Column(name = "ID")
//	private Short id;
//
//	@Column(name="name")
//	private String name;
//
//	@Column(name="health_category_id")
//	private HealthCategory category;
//
//	@Column(name="photo_file_path")
//	private String photoFilePath;
//
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "trainers", cascade = CascadeType.ALL)
//	List <Trainer> trainerList;
//
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "groups", cascade = CascadeType.ALL)
//	List <Group> groupList;
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((id == null) ? 0 : id.hashCode());
//		result = prime * result + ((name == null) ? 0 : name.hashCode());
//		result = prime * result + ((category == null) ? 0 : category.hashCode());
//		result = prime * result + ((trainerList == null) ? 0 : trainerList.hashCode());
//		result = prime * result + ((groupList == null) ? 0 : groupList.hashCode());
//		result = prime * result + ((photoFilePath == null) ? 0 : photoFilePath.hashCode());
//
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if(this == obj)
//			return true;
//		if(obj == null)
//			return false;
//		if(getClass() != obj.getClass())
//			return false;
//		Specialization other = (Specialization) obj;
//		if(id == null) {
//			if(other.id != null)
//				return false;
//		} else if(!id.equals(other.id))
//			return false;
//		if(name == null) {
//			if(other.name != null)
//				return false;
//		} else if(!name.equals(other.name))
//			return false;
//		if(category == null) {
//			if(other.category != null)
//				return false;
//		} else if(!category.equals(other.category))
//			return false;
//		if(photoFilePath == null) {
//			if(other.photoFilePath != null)
//				return false;
//		} else if(!photoFilePath.equals(other.photoFilePath))
//			return false;
//		if(trainerList == null) {
//			if(other.trainerList != null)
//				return false;
//		} else if(!trainerList.equals(other.trainerList))
//			return false;
//		if(groupList == null) {
//			if(other.groupList != null)
//				return false;
//		} else if(!groupList.equals(other.groupList))
//			return false;
//
//		return true;
//	}
//
//	@Override
//	public String toString() {
//		return "Specialization [id=" + id + ", name=" + name + ", min. approvable health category=" + category.getId() +
//				", trainers=" + Arrays.toString(trainerList.toArray()) + ", groups=" + Arrays.toString(groupList.toArray()) + "]";
//	}
//}
