-- Role --
DROP TABLE IF EXISTS Submissions;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Roles ;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS Trainers;
DROP TABLE IF EXISTS Specializations;
DROP TABLE IF EXISTS Health_Categories ;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS Trainers;

CREATE TABLE Roles (
	id SMALLINT PRIMARY KEY,
	name VARCHAR(30) DEFAULT NULL
);

-- Health category --
CREATE TABLE Health_Categories (
	id SMALLINT PRIMARY KEY,
	name VARCHAR(30) DEFAULT NULL
);

-- Specialization --
CREATE SEQUENCE specializationIdSequence;
CREATE TABLE Specializations (
	id SMALLINT NOT NULL DEFAULT nextval('specializationIdSequence') PRIMARY KEY,
	name VARCHAR(50) DEFAULT NULL,
	health_category_id SMALLINT REFERENCES health_categories(id),
	photo_file_path VARCHAR(50) DEFAULT NULL
);

-- Trainer --
CREATE SEQUENCE trainer_id_sequence;
CREATE TABLE Trainers (
	id serial PRIMARY KEY ,
	last_name VARCHAR(30) DEFAULT NULL,
	first_name VARCHAR(30) DEFAULT NULL,
	second_name VARCHAR(30) DEFAULT NULL
	--specialization_id SMALLINT REFERENCES specializations(id),
	--photo_file_path VARCHAR(50) DEFAULT NULL
);
DROP TABLE IF EXISTS Submissions;
CREATE TABLE Submissions (
    id serial PRIMARY KEY ,
    firstName VARCHAR(64) DEFAULT NULL,
    secondName VARCHAR(64) DEFAULT NULL,
    lastName VARCHAR(64) DEFAULT NULL,
    groupNumber VARCHAR(64) DEFAULT NULL,
    dayOfWeek VARCHAR(64) DEFAULT NULL,
    timeInterval VARCHAR(64) DEFAULT NULL,
    sportTypeName VARCHAR(64) DEFAULT NULL,
    trainer VARCHAR(128) DEFAULT NULL,
    location VARCHAR(64) DEFAULT NULL,
    status BOOLEAN DEFAULT NULL,
    userId BIGINT NOT NULL
);
Insert into Submissions values (1,'A','B','C','12354321','Wednesday','10.00-11.40','Water Polo','Kabanov Aleksey Aleksandrovich','Svetlanovskaya Ploshad',DEFAULT,1);
select * from submissions;
-- Group schedule --
CREATE SEQUENCE scheduleIdSequence;
CREATE TABLE Schedules (
	id SMALLINT DEFAULT nextval('scheduleIdSequence') PRIMARY KEY,
	week SMALLINT NOT NULL,
	day SMALLINT NOT NULL,
	start_at TIME,
	finish_at TIME
);

-- Location --
CREATE SEQUENCE locationIdSequence;
CREATE TABLE Locations (
	id SMALLINT NOT NULL DEFAULT nextval('locationIdSequence') PRIMARY KEY,
	address VARCHAR(255) DEFAULT NULL,
	photo_file_path VARCHAR(50) DEFAULT NULL
);

-- Group --
CREATE SEQUENCE groupIdSequence;
CREATE TABLE Groups (
	id BIGINT NOT NULL DEFAULT nextval('groupIdSequence') PRIMARY KEY,
	group_specialization_id SMALLINT REFERENCES specializations(id),
	trainer_id SMALLINT REFERENCES trainers(id),
	schedule_id SMALLINT REFERENCES schedules(id),
	location_id SMALLINT REFERENCES locations(id),
	target_size SMALLINT NOT NULL,
	actual_size SMALLINT NOT NULL,
	registration_status VARCHAR(20) DEFAULT NULL,
	description VARCHAR(255) DEFAULT NULL
);

-- User --
CREATE SEQUENCE userIdSequence;
CREATE TABLE Users (
	id BIGINT NOT NULL DEFAULT nextval('userIdSequence') PRIMARY KEY,
	user_role_id SMALLINT REFERENCES roles(id),
	last_name VARCHAR(30) DEFAULT NULL,
	first_name VARCHAR(30) DEFAULT NULL,
	second_name VARCHAR(30) DEFAULT NULL,
	user_group_id BIGINT REFERENCES groups(id),
	user_health_category SMALLINT REFERENCES health_categories(id),
	confirmed_on TIMESTAMP,
	photo_file_path VARCHAR(50) DEFAULT NULL
);

-- Submission --
CREATE SEQUENCE submissionIdSequence;
CREATE TABLE Submissions (
	id BIGINT NOT NULL DEFAULT nextval('submissionIdSequence') PRIMARY KEY,
	status VARCHAR(30) DEFAULT NULL,
	accepted TIMESTAMP,
	last_update TIMESTAMP,
	user_id BIGINT REFERENCES users(id),
	confirmed_group_id BIGINT REFERENCES groups(id)
);







