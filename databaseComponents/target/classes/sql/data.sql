-- Roles --
INSERT INTO ROLES VALUES (0, 'Пользователь ЛК');
INSERT INTO ROLES VALUES (1, 'Администратор');

-- Health categories --
INSERT INTO HEALTH_CATEGORIES VALUES (0, 'Специальная');
INSERT INTO HEALTH_CATEGORIES VALUES (1, 'Основная');

-- Specializations -
INSERT INTO SPECIALIZATIONS VALUES (1, 'Аэробика', 0, '../somepath');
INSERT INTO SPECIALIZATIONS VALUES (2, 'ОФП', 0, '../somepath');
INSERT INTO SPECIALIZATIONS VALUES (3, 'Спортивные игры', 1, '../somepath');
INSERT INTO SPECIALIZATIONS VALUES (4, 'Бокс', 1, '../somepath');
INSERT INTO SPECIALIZATIONS VALUES (5, 'Борьба', 1, '../somepath');

-- Trainers --
INSERT INTO Trainers VALUES (1, 'Бушма', 'Татьяна', 'Валерьевна', 1, '../somepath');
INSERT INTO Trainers VALUES (2, 'Васильева', 'Валерия', 'Сергеевна', 2, '../somepath');
INSERT INTO Trainers VALUES (3, 'Спиридонова', 'Лариса', 'Евгеньевна', 3, '../somepath');
INSERT INTO Trainers VALUES (4, 'Щадрин', 'Игорь', 'Николаевич', 3, '../somepath');
INSERT INTO Trainers VALUES (5, 'Щарнин', 'Николай', 'Павлович', 4, '../somepath');

-- Schedules --
INSERT INTO Schedules VALUES (1, 0, 1, '12:00', '13:40');
INSERT INTO Schedules VALUES (2, 0, 2, '08:00', '09:40');
INSERT INTO Schedules VALUES (3, 1, 1, '14:00', '15:40');
INSERT INTO Schedules VALUES (4, 1, 4, '16:00', '17:40');
INSERT INTO Schedules VALUES (5, 1, 5, '10:00', '11:40');

-- Locations --
INSERT INTO Locations VALUES (1, 'Парголовская ул., 8', '../somepath');
INSERT INTO Locations VALUES (2, 'Политехническая ул., 27', '../somepath');
INSERT INTO Locations VALUES (3, 'Энгельса пр., 23', '../somepath');

-- Groups --
INSERT INTO Groups VALUES (1, 1, 1, 2, 2, 24, 11, 'закрыта', 'К занятиям в группе приглашаются студенты...');
INSERT INTO Groups VALUES (2, 2, 3, 4, 1, 30, 18, 'открыта', 'К занятиям в группе приглашаются студенты...');
INSERT INTO Groups VALUES (3, 2, 4, 5, 3, 28, 2, 'закрыта', 'К занятиям в группе приглашаются студенты...');
INSERT INTO Groups VALUES (4, 5, 2, 4, 1, 30, 14, 'открыта', 'К занятиям в группе приглашаются студенты...');
INSERT INTO Groups VALUES (5, 2, 3, 5, 2, 26, 26, 'закрыта', 'К занятиям в группе приглашаются студенты...');

-- User --
INSERT INTO Users VALUES (1, 0, 'Задов', 'Василий', 'Петрович', 2, 0, '2019-10-16 19:32:55', '../somepath');
INSERT INTO Users VALUES (2, 0, 'Тракторенко', 'Пётр', 'Степанович', 4, 1, '2019-09-05 11:57:02', '../somepath');
INSERT INTO Users VALUES (3, 1, 'Пастушенко', 'Тарас', 'Гаврилович', 1, 1, NULL, NULL);
INSERT INTO Users VALUES (4, 0, 'Кайло', 'Григорий', 'Кондратьевич', 3, 1, '2019-11-01 17:07:21', '../somepath');
INSERT INTO Users VALUES (5, 0, 'Клюшкина', 'Екатерина', 'Александровна', 1, 1, '2019-09-25 10:07:41', '../somepath');

-- Submission --
INSERT INTO submissions VALUES (1, 'отмена - Админ', '2019-10-02 19:01:10', '2019-10-03 11:59:14', 2, 2);
INSERT INTO submissions VALUES (2, 'активна', '2019-10-03 15:23:16', '2019-10-05 17:53:25', 4, 2);
INSERT INTO submissions VALUES (3, 'отмена - ЛК', '2019-10-07 14:19:32', '2019-10-15 17:21:57', 1, 5);
INSERT INTO submissions VALUES (4, 'активна', '2019-10-30 10:18:37', '2019-10-30 10:18:37', 2, 4);
INSERT INTO submissions VALUES (5, 'активна', '2019-11-06 20:18:55', '2019-11-06 20:18:55', 1, 5);


select * from submissions;

