package com.eureka.image.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Submissions")
@Getter @Setter @NoArgsConstructor
public class Submission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="firstname")
    private String firstName;
    @Column(name="secondname")
    private String secondName;
    @Column(name="lastname")
    private String lastName;
    @Column(name="groupnumber")
    private String groupNumber;
    @Column(name="dayofweek")
    private String dayOfWeek;
    @Column(name="timeinterval")
    private String timeInterval;
    @Column(name="sporttypename")
    private String sportTypeName;
    @Column(name="trainer")
    private String trainer;
    @Column(name="location")
    private String location;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "userid")
    private Long userId;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((secondName == null) ? 0 : secondName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
//        result = prime * result + ((groupNumber == null) ? 0 : groupNumber.hashCode());
//        result = prime * result + ((dayOfWeek == null) ? 0 : dayOfWeek.hashCode());
//        result = prime * result + ((timeInterval == null) ? 0 : timeInterval.hashCode());
//        result = prime * result + ((sportTypeName == null) ? 0 : sportTypeName.hashCode());
//        result = prime * result + ((trainer == null) ? 0 : trainer.hashCode());
//        result = prime * result + ((location == null) ? 0 : location.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Submission other = (Submission) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (secondName == null) {
            if (other.secondName != null)
                return false;
        } else if (!secondName.equals(other.secondName))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
//        if (groupNumber == null) {
//            if (other.groupNumber != null)
//                return false;
//        } else if (!groupNumber.equals(other.groupNumber))
//            return false;
//        if (dayOfWeek == null) {
//            if (other.dayOfWeek != null)
//                return false;
//        } else if (!dayOfWeek.equals(other.dayOfWeek))
//            return false;
//        if (timeInterval == null) {
//            if (other.timeInterval != null)
//                return false;
//        } else if (!timeInterval.equals(other.timeInterval))
//            return false;
//        if (sportTypeName == null) {
//            if (other.sportTypeName != null)
//                return false;
//        } else if (!sportTypeName.equals(other.sportTypeName))
//            return false;
//        if (trainer == null) {
//            if (other.trainer != null)
//                return false;
//        } else if (!trainer.equals(other.trainer))
//            return false;
//        if (location == null) {
//            if (other.location != null)
//                return false;
//        } else if (!location.equals(other.location))
//            return false;
        return true;
    }
    @Override
    public String toString() {
        return "Trainer [id=" + id + ", first name=" + firstName  +
                ", last name =" + lastName  + "]";
    }
}
