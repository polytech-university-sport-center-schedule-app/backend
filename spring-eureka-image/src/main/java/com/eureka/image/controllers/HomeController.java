package com.eureka.image.controllers;

import java.util.List;
import java.util.stream.Collectors;

import com.eureka.image.entities.Submission;
import com.eureka.image.repository.SubmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import com.eureka.image.entities.Image;

@RestController
@RequestMapping("/submission")
public class HomeController {
	@Autowired
	private Environment env;

	@Autowired
	private SubmissionRepository submissionRepository;
		
	@GetMapping()
	public List<Submission> getSubmissions(@RequestParam(name = "userId") Long userId) {
		return ((List<Submission>)submissionRepository.findAll()).stream()
																 .filter(submission -> submission.getUserId()==userId)
																 .collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public Submission getSubmission(@PathVariable Long id) {
		return submissionRepository.findById(id).orElseThrow(() -> new NullPointerException());
	}

	@PostMapping
	public Submission addSubmission(@RequestParam(name = "userId") Long userId, @RequestBody Submission submission) {
		submission.setUserId(userId);
		return submissionRepository.save(submission);
	}
}
